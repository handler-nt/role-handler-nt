const assert = require('assert');
//const expect = require('assert').expect;
//const should = require('chai').should();

const lib = require('../index.js');

describe('General working', () =>
{
  it("Check role permission", () =>
  {

    lib.inMemoryGo(
    [
      {
        label: "admin",
        modules:
        {
          contact: "111111"
        }
      }
    ])
    .then(() =>
    {
      assert.equal(lib.check("admin", "contact", "create"), true);
      assert.equal(lib.check("admin", "contact", 2), true);
    })
    .catch(err =>
    {
      console.error(err);
    })
  });
});
