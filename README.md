# RoleHandler
Simple module that is caching roles using lokijs. 

## Installation
$ <code>yarn install role-handler-nt --save</code>

## Documentation

- <code>inMemoryGo(arrRoles)</code><br>
	* Description : Caching the roles.<br>
	* @param {Array} arrRoles<br>
  * @return {Promise}
 
- <code>check(roleLabel, module, action)</code><br>
	 * Description : Check the role
   * @param {string} roleLabel
   * @param {string} module
   * @param {string | number} action
   * @return {boolean}
	