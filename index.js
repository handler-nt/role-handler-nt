const loki  = require('lokijs');
const db = new loki('./test.db');
const roles = db.addCollection('roles');

const CRUDIE =
{
  'create': 0,
  'read': 1,
  'update': 2,
  'delete': 3,
  'import': 4,
  'export': 5
};


/**
 * Caching the roles
 * @param {Array} arrRoles :
 * @return {Promise}
 */
async function inMemoryGo(arrRoles)
{
  return new Promise((resolve, reject) =>
  {
    arrRoles.forEach((role) =>
    {
      roles.insert(role);
    });

    return resolve();
  })

}

/**
 * Check the role
 * @param {string} roleLabel
 * @param {string} module
 * @param {string | number} action
 * @return {boolean}
 */
function check(roleLabel, module, action)
{
  let role = roles.findOne({label: roleLabel});

  if (role)
  {
    if (role.modules.hasOwnProperty(module))
    {
      if (typeof (action) === "string")
        action = parseInt(CRUDIE[action]);

      if (typeof (action) === "number")
        return (action < 6 && role.modules[module].charAt(action) === "1");

      return false;
    }

    return false;
  }

  return false;
}


module.exports =
{
  inMemoryGo,
  check
};